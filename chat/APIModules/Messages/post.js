const Joi = require('joi');
var jsonwebtoken = require('jsonwebtoken');
var ComnFun = require('../../ComnFun.js');
var conf = require('../../conf.json');
var secretKey = conf.secretKey;
var middleWare = require("../../Controller/dbMiddleware.js");
var phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
var async = require("async");
var objectId = require("mongodb").ObjectID;
const moment = require('moment');

module.exports = [{
    method: 'POST',
    path: '/Messages/web',
    config: {
        handler: function (req, reply) {
            postMessageHandler(req, reply)
        },
        validate: {
            /**
             * type: 0: offer
             * offerType: if 'type == 15' then only | 1 - offer made, 2 - counter offer, 0 - accepted
             */
            payload: Joi.object({
                name: Joi.string().description("name is required"),
                from: Joi.string().description("from is required"),
                to: Joi.string().description("to is required"),
                payload: Joi.string().description("payload is required"),
                type: Joi.string().description("type is required"),
                id: Joi.string().description("id is required"),
                secretId: Joi.string().description("secretId is required"),
                thumbnail: Joi.any().default('').allow().description("thumbnail is required"),
                dataSize: Joi.number().description("dataSize is required"),
            }).unknown(),
            headers: Joi.object({
                'authorization': Joi.string().description("required")
            }).unknown()
        },
        description: 'Post a message',
        notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
        tags: ['api']
    }

}];

function postMessageHandler(req, reply) {

    console.log('postMessageHandler: ', req.payload);
    let chat = [];
    async.waterfall([
        function (validateCB) {
            /**
             * validate the input
             */
            if (!req.headers.authorization) {
                return reply({
                    code: 101,
                    message: "mandatory headers is missing"
                }).code(200);
            } else {
                if (req.headers.authorization != conf.authorization) {
                    return reply({
                        code: 102,
                        message: "failed to authenticate, headers is Invalid "
                    }).code(200);
                }
            }

            if (!req.payload.from) {
                return reply({
                    code: 103,
                    message: "mandatory 'from' is missing"
                }).code(200);
            }
            if (!req.payload.to) {
                return reply({
                    code: 103,
                    message: "mandatory 'to' is missing"
                }).code(200);
            }
            if (!req.payload.payload) {
                return reply({
                    code: 103,
                    message: "mandatory 'payload' is missing"
                }).code(200);
            }
            if (!req.payload.type) {
                return reply({
                    code: 103,
                    message: "mandatory 'type' is missing"
                }).code(200);
            }
            if (!req.payload.id) {
                return reply({
                    code: 103,
                    message: "mandatory 'id' is missing"
                }).code(200);
            }
            if (!req.payload.secretId) {
                return reply({
                    code: 103,
                    message: "mandatory 'secretId' is missing"
                }).code(200);
            }
            if (!req.payload.name) {
                return reply({
                    code: 103,
                    message: "mandatory 'name' is missing"
                }).code(200);
            }


            /**
             * All Ok. Continue
             */
            validateCB(null)
        },
        function (funcMainCB) {

            var userExists = "members." + req.payload.from;
            var targetExists = "members." + req.payload.to;
            var condition = {
                [userExists]: {
                    $exists: true
                },
                [targetExists]: {
                    $exists: true
                },
                secretId: req.payload.secretId
            };

            /**
             * Check if the ChatList exixts or not
             */
            middleWare.Select("chatList", condition, "Mongo", {}, function (err, result) {
                if (err) {
                    return reply({
                        message: err.message
                    }).code(500);
                } else if (result.length > 0) {
                    chat = result[0];
                    (result[0]) ? funcMainCB(null, result[0]._id) : funcMainCB(null, 0);
                } else {
                    chat['toDocId'] = '';
                    funcMainCB(null, "");
                }
            });

        },
        function (chatId, messageDb) {
            if (chatId) {
                let condition = {
                    chatId: objectId(chatId)
                }
                middleWare.Select("messages", condition, "Mongo", {
                    sort: {
                        _id: -1
                    }
                }, function (err, result) {
                    if (err) {
                        return reply({
                            message: err.message
                        }).code(500);
                    } else if (result.length > 0) {
                        chat.toDocId = result[0].toDocId;
                        messageDb(null, chatId);
                    } else {
                        // chat.toDocId = );
                        messageDb(null, chatId);

                    }
                });
            } else {
                // chat.toDocId = "";
                messageDb(null, chatId);
            }
        },
        function (chatId, finalCB) {
            /**
             * type: 15-offer
             */
            var name, userId, targetUserId, payload, type, messageId, creation, dataSize, secretId, thumbnail, dTime, userImage, toDocId, offerType;
            var prodName, prodImage, prodId, prodPrice, isSold;
            let msg_mqtt_data = {
                receiverIdentifier: '',
                name: req.payload.name,
                from: req.payload.to,
                to: req.payload.from,
                payload: req.payload.payload,
                type: req.payload.type,
                userImage: '',
                dataSize: req.payload.dataSize || -1,
                thumbnail: req.payload.thumbnail || '',
                timestamp: (moment().valueOf()).toString(),
                id: (moment().valueOf()).toString(),
                secretId: (req.payload.secretId) ? req.payload.secretId : "",
                dTime: -1,
                toDocId: chat.toDocId || (moment().valueOf()).toString(),
            }
            console.log("to", "Message/", msg_mqtt_data);
            // ComnFun.publishMqtt("Message/" + req.payload.to, JSON.stringify(msg_mqtt_data));
            ComnFun.publishMqtt("Message/" + req.payload.from, JSON.stringify(msg_mqtt_data));


            // ComnFun.publishMqtt("Message/" + targetUserId, JSON.stringify(msg_mqtt_data));
            return reply({
                message: "message sent successfully",
                data: msg_mqtt_data
            }).code(200);

        }
    ])

}